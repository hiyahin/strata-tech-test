const actions = (state = [], action) => {
  switch (action.type) {
    case 'SET_TFL_MODES':
      return action.response
    default:
      return state
  }
}

export default actions