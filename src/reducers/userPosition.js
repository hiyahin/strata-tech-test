const actions = (state = {}, action) => {
  switch (action.type) {
    case 'SET_GEOLOCATION':
      return action.position
    default:
      return state
  }
}

export default actions