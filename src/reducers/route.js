const initialState = {
	route: 'MainMenu',
	params: {}
}

const actions = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_ROUTE':
      return {
      	route: action.route,
      	params: action.params
      }
    case 'SET_ROUTE_TO_HOME' :
    	return initialState
    default:
      return state
  }
}

export default actions