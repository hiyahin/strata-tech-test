//initialState should loop through a constant rather than hardcode here
const initialState = {
	tube: [],
	dlr: [],
	overground: []
}

const actions = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_DISRUPTIONS':
    	let newState = Object.assign({}, state);
    	newState[action.mode] = action.response;
      return newState
    default:
      return state
  }
}

export default actions