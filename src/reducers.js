import { combineReducers } from 'redux';
import userPosition from './reducers/userPosition';
import tflModes from './reducers/tflModes';
import disruptions from './reducers/disruptions';
import route from './reducers/route';

const mainReducer = combineReducers({
	userPosition,
	tflModes,
	disruptions,
	route
})

export default mainReducer