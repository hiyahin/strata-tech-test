import './scss/index.scss'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import mainReducer from './reducers.js'
import App from './containers/App'

let store = createStore(
	mainReducer,
	applyMiddleware(
		thunkMiddleware
	)
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)