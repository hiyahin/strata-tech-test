import React from 'react';
import { connect } from 'react-redux';
import { backToMainMenu } from './actions';
import StationList from '../../components/StationList';

class ModeDetail extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			stations: []
		}
	}

	componentDidMount() {
		//Use user position
		let userPosition = this.props.userPosition;
		fetch('https://api.tfl.gov.uk/Place?type=NaptanMetroStation,NaptanRailStation&lat='+ userPosition.lat + '&lon=' + userPosition.long + '&radius=800').
		then(response => response.json())
		.then(response => this.setState({
			stations: response.places
				.filter((item) => {
					//Only allow stations that has current mode of transport on it
					return item.modes.indexOf(this.props.params.type) > -1
				})
				.sort((a,b) => {
					return a.distance > b.distance
				})
				.slice(0, 5)
		}));
	}

  render() {
  	let disruptions, disruptionList;
  	let modeName = this.props.params.type;

  	if (this.props.disruptions[modeName] !== undefined && this.props.disruptions[modeName].length) {
  		disruptionList = this.props.disruptions[modeName].map((disruption, index) => {
  			//May need better way to remove html tags
  			return <li key={index}>{disruption.description.replace(/<\/?[^>]+(>|$)/g, "")}</li>
  		});
  		disruptions = (<div className="alert alert-warning"><ul>{disruptionList}</ul></div>)
  	} 
  	else {
  		disruptions = (<div className="alert alert-info"><p>There are currently no disruptions</p></div>)
  	}
  	return (
  		<div>
	  		<a onClick={() => this.props.backToMainMenu()}>Back</a>
	  		<h2>Mode Detail: {modeName}</h2>
	  		<h3>Disruptions</h3>
	  		{disruptions}
	  		<div>
	  			<StationList stations={this.state.stations} />
	  		</div>
  		</div>
  	)
  }
}

const mapStateToProps = state => {
	return {
		disruptions: state.disruptions,
		userPosition: state.userPosition
	}
}

const mapDispatchToProps = dispatch => {
	return {
		backToMainMenu: () => {
			dispatch(backToMainMenu());
		}
	}
}

const connectedModeDetail = connect (
	mapStateToProps,
	mapDispatchToProps
)(ModeDetail)

export default connectedModeDetail