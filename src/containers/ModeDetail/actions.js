export const backToMainMenu = () => {
	return {
		type: 'SET_ROUTE_TO_HOME'
	}
}