import React from 'react';
import { connect } from 'react-redux';
import MainMenu from '../MainMenu';
import ModeDetail from '../ModeDetail';

class App extends React.Component {
  render() {
  	let activeComponent;
		switch(this.props.route.route) {
			case 'ModeDetail':
				activeComponent = <ModeDetail params={this.props.route.params} />
				break;
			default:
				activeComponent = <MainMenu params={this.props.route.params} />
		}
    return (
			<div>
				{activeComponent}
			</div>
    )
  }
}

const mapStateToProps = state => {
	return {
		userPosition: state.userPosition,
		route: state.route
	}
}

const mapDispatchToProps = dispatch => {
	return {
	}
}

const connectedApp = connect (
	mapStateToProps,
	mapDispatchToProps
)(App)

export default connectedApp