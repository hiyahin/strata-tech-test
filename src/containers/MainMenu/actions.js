export const getUserPosition = () => {
	return dispatch => {
		navigator.geolocation.getCurrentPosition((position) => {
			dispatch(setUserPosition(position));
		});
	}
};

const setUserPosition = position => {
	return {
		type: 'SET_GEOLOCATION',
		position: {
			lat: position.coords.latitude,
			long: position.coords.longitude
		}
	}
}

export const getTflModes = () => {
	return dispatch => {
		fetch('https://api.tfl.gov.uk/Line/Meta/Modes')
		.then(response => response.json())
		.then(response => dispatch(setTFLModes(response)))
	}
}

const setTFLModes = response => {
	return {
		type: 'SET_TFL_MODES',
		response: response
	}
}

export const getDisruptions = (type) => {
	return dispatch => {
		fetch('https://api.tfl.gov.uk/Line/Mode/' + type + '/Disruption')
		.then(response => response.json())
		.then(response => dispatch(setDisruptions(response, type)))
	}
}

const setDisruptions = (response, type) => {
	return {
		type: 'SET_DISRUPTIONS',
		response: response,
		mode: type
	}
}