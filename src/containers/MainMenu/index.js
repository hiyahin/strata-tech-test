import React from 'react';
import { connect } from 'react-redux';
import { getUserPosition, getTflModes, getDisruptions } from './actions';
import TflModesList from '../../components/TflModesList';

//Extract as external constant
const disruptionModes = [
	'tube',
	'dlr',
	'overground'
];

class MainMenu extends React.Component {

	componentDidMount() {
		this.props.getUserPosition();
		this.props.getTflModes();

		//Can get all disruptions in one call, but cannot separate the types, so calling separately
		disruptionModes.forEach((type) => this.props.getDisruptions(type));
	}

  render() {
    return (
			<div>
				<h1>Strata Test</h1>
				<p>Your current location: {this.props.userPosition.lat}, {this.props.userPosition.long}</p>
				<TflModesList />
			</div>
    )
  }
}

const mapStateToProps = state => {
	return {
		userPosition: state.userPosition
	}
}

const mapDispatchToProps = dispatch => {
	return {
		getUserPosition: () => {
			dispatch(getUserPosition());
		},
		getTflModes: () => {
			dispatch(getTflModes());
		},
		getDisruptions: type => {
			dispatch(getDisruptions(type));
		}
	}
}

const connectedMainMenu = connect (
	mapStateToProps,
	mapDispatchToProps
)(MainMenu)

export default connectedMainMenu