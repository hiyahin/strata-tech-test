import React from 'react';
import { connect } from 'react-redux';
import { goToModeDetail } from './actions'

//Add Title to highlight number of incidents?
const warningIcon = (disruptions) => {
	if (Array.isArray(disruptions)) {
		if (disruptions.length > 0) {
			return <span className={"badge badge-warning badge-pill"}>!</span>
		}
		else {
			return <span className={"badge badge-success badge-pill"}>✔</span>
		}
	}
}

const tflModesList = (props) => {
	let tflModesItems = props.modes.map((mode, index) => {
		return <li
			className="list-group-item justify-content-between list-group-item-action"
			key={index}
			onClick={() => props.goToModeDetail(mode.modeName)}
			>
				{mode.modeName} {warningIcon(props.disruptions[mode.modeName])}
			</li>
	})
	return (
		<ul className="list-group">
			{tflModesItems}
		</ul>
	)
}

const mapStateToProps = state => {
	return {
		modes: state.tflModes,
		disruptions: state.disruptions,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		goToModeDetail: (type) => {
			dispatch(goToModeDetail(type));
		}
	}
}

const connectedTflModesList = connect (
	mapStateToProps,
	mapDispatchToProps
)(tflModesList)

export default connectedTflModesList