export const goToModeDetail = (type) => {
	return {
		type: 'SET_ROUTE',
		route: 'ModeDetail',
		params: {type: type}
	}
};
