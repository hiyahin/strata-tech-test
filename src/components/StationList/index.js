import React from 'react';
import { connect } from 'react-redux';

const stationList = (props) => {
	let stationItems = props.stations.map((station, index) => {
		let modeList = station.modes.map((mode, index) => {
			let lineList = station.lineModeGroups
				.filter((item) => {
					return item.modeName == mode
				})[0]
				.lineIdentifier.join(', ')

			return (
				<li key={index} className="list-group-item">
					{mode} - {lineList}
				</li>
			)
		})
		return <li
			className="list-group-item"
			key={index}
			>
				{station.commonName} ({Math.round(station.distance)}m away)
				<ul className="list-group">
					{modeList}
				</ul>
			</li>
	})
	return (
		<ul className="list-group">
			{stationItems}
		</ul>
	)
}

export default stationList